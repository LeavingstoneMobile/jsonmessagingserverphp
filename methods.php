<?php

abstract class Methods
{
    const DummyMethod = 0;
    const DummyMethodResult =1;

    const RegisterUser = 10;
    const RegisterUserResult = 11;

    const AuthorizeUser = 12;
    const AuthorizeUserResult = 13;

    const CreateUserRole = 14;
    const CreateUserRoleResult = 15;

    const GetAvailableRoles = 16;
    const GetAvailableRolesResult = 17;
    
    const AssignRolesToUser = 18;
    const AssignRolesToUserResult = 19;

    const SessionIsValid = 20;
    const SessionIsValidResult = 21;

    const DeleteUser = 22;
    const DeleteUserResult = 23;

    const DeleteRole = 24;
    const DeleteRoleResult = 25;

    const GetUsers = 26;
    const GetUsersResult = 27;
}   