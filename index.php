<?php

require_once 'methods.php';
require_once 'methodhandlers.php';

function createMethodHandlersTable()
{
    $methodHandlerTable = array();

    $methodHandlerTable[Methods::RegisterUser] = "RegisterUser";
    $methodHandlerTable[Methods::AuthorizeUser] = "AuthorizeUser";    

    return $methodHandlerTable;
}

function main()
{
    $methodHandlerTable = createMethodHandlersTable();

    $postdata = file_get_contents("php://input");   

    $requestJSONString = $postdata;

    $requestJSON = json_decode($requestJSONString);
   
    if(!isset($requestJSON->method))
    {

        header("Status-Code = ".(string)500);    

        var_dump($methodHandlerTable); 

        // ეს შემთხვევა დასამუშავებელია შესაბამისი JSONMessage-ით
        // თუმცა რახან შეიძლება RequestJSON.method-ის გარდა
        // RequstJSON.messageId-ც არ გვქონდეს, გამოვიდეთ მხოლოდ http code 500-ით,
        // უბრალოდ იმისთვის საშუალება ქონდეს კლიენტის მხარეს დაიხმაუროს პრობლემის შესახებ
        return;
    }

    $methodHandler = $methodHandlerTable[$requestJSON->method];

    $responseJSON = $methodHandler($requestJSON);

    $responseJSONString = json_encode($responseJSON);

    printf($responseJSONString);
}

main();